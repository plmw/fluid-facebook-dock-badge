// ==UserScript==
// @name        Facebook Dock Badge
// @namespace   http://fluidapp.com
// @description Display a count of the messages, friend requests and notifications in the dock badge for Fluid.
// @include     *.facebook.com/*
// @author      Phillip Wong
// ==/UserScript==

var debug = false;

var fbParser = (function () {
	function updateBadge() {
		var stripHTMLExpression = new RegExp("\<[^\>]*\>","g");

		var notificationsElementHTML = document.getElementById('notificationsCountValue').innerHTML;
		var notificationsCount = parseInt(notificationsElementHTML.replace(stripHTMLExpression, ''));

		var messagesElementHTML = document.getElementById('mercurymessagesCountValue').innerHTML;
		var messagesCount = parseInt(messagesElementHTML.replace(stripHTMLExpression, ''));

		var requestsElementHTML = document.getElementById('requestsCountValue').innerHTML;
		var requestsCount = parseInt(requestsElementHTML.replace(stripHTMLExpression, ''));

		var totalCount = notificationsCount + messagesCount + requestsCount;

		if (debug) {
			window.console.log('Debug: notifications: "' + notificationsCount + '"');
			window.console.log('Debug: messages: "' + messagesCount + '"');
			window.console.log('Debug: requests: "' + requestsCount + '"');
			window.console.log('Debug: total: "' + totalCount + '"');
		}

		if (totalCount == 0) {
			totalCount = ''; //if the total count is 0, don't display the dockbadge
		}

		window.fluid.dockBadge = totalCount;
	};
	
	return {
		init : function() {
							updateBadge();
	    					window.setInterval(updateBadge, 2000); //poll every couple of seconds
			   }
	};
}());

var readyStateCheckInterval = setInterval(function() {
    if (document.readyState === "complete") {
        if (debug) { window.console.log('Debug: document ready, initialising...'); }
        
		fbParser.init();
		
        clearInterval(readyStateCheckInterval);
    }
}, 50);
